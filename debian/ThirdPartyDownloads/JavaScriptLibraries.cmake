set(JAVASCRIPT_LIBS_DIR  ${CMAKE_CURRENT_BINARY_DIR}/javascript-libs)
file(MAKE_DIRECTORY ${JAVASCRIPT_LIBS_DIR})

# We take "axios", "bootstrap", "fontawesome", and "vue.js" from the
# official Debian packages. We ship our own version of "babel polyfill"
# (not packaged yet).

file(COPY
  ${CMAKE_SOURCE_DIR}/debian/ThirdPartyDownloads/babel-polyfill/polyfill.min.js
  /usr/share/bootstrap-html/js/bootstrap.min.js
  /usr/share/javascript/vue/vue.min.js
  /usr/share/nodejs/axios/dist/axios.min.js
  /usr/share/nodejs/axios/dist/axios.min.js.map
  DESTINATION
  ${JAVASCRIPT_LIBS_DIR}/js
  )

file(COPY
  /usr/share/bootstrap-html/css/bootstrap.min.css
  /usr/share/bootstrap-html/css/bootstrap.min.css.map
  /usr/share/fonts-font-awesome/css/font-awesome.min.css
  DESTINATION
  ${JAVASCRIPT_LIBS_DIR}/css
  )

file(COPY
  /usr/share/fonts-font-awesome/fonts/fontawesome-webfont.eot
  /usr/share/fonts-font-awesome/fonts/fontawesome-webfont.svg
  /usr/share/fonts-font-awesome/fonts/fontawesome-webfont.woff
  /usr/share/fonts-font-awesome/fonts/fontawesome-webfont.woff2
  /usr/share/fonts/opentype/font-awesome/FontAwesome.otf
  /usr/share/fonts/truetype/font-awesome/fontawesome-webfont.ttf
  DESTINATION
  ${JAVASCRIPT_LIBS_DIR}/fonts
  )

file(COPY
  ${CMAKE_SOURCE_DIR}/Resources/Orthanc/OrthancLogo.png
  DESTINATION
  ${JAVASCRIPT_LIBS_DIR}/img
  )
