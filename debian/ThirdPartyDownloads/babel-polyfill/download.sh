#!/bin/bash
set -ex

npm install --save-dev babel-polyfill
mv node_modules/babel-polyfill/dist/polyfill.js .
mv node_modules/babel-polyfill/dist/polyfill.min.js .
rm -rf node_modules package-lock.json
